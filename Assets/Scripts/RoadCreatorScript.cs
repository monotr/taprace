﻿using UnityEngine;
using System.Collections;

public class RoadCreatorScript : MonoBehaviour {
	public GameObject step;

	void Start () {
		road420 ();
	}
	
	void road420 () {
		GameObject parent = new GameObject ();
		parent.name = "roadParent";
		int i = 0;


		float y = 0;
		for(float j = -5.2f; j < 5.2f; j += (5.2f*2)/100){
			/*y = 2 * Mathf.Sqrt (-1 * Mathf.Abs (Mathf.Abs ((float)x) - 1) * Mathf.Abs (3 - Mathf.Abs ((float)x)) / ((Mathf.Abs ((float)x) - 1) *
				(3 - Mathf.Abs ((float)x)))) * (1 + Mathf.Abs (Mathf.Abs ((float)x) - 3) / (Mathf.Abs ((float)x) - 3)) *
				Mathf.Sqrt (1 - Mathf.Pow (((float)x / 7), 2)) + (5 + 0.97f (Mathf.Abs ((float)x - 0.5f) + Mathf.Abs ((float)x + 0.5f)) -
					3 (Mathf.Abs ((float)x - 0.75f) + Mathf.Abs ((float)x + 0.75f))) * (1 + Mathf.Abs (1 - Mathf.Abs ((float)x)) /
						(1 - Mathf.Abs ((float)x)));*/

			//curves
			//y = Mathf.Sqrt(1- Mathf.Pow(Mathf.Abs(Mathf.Abs(j) - 2) - 1, 2));

			//batman parte 1
			//y= 7 * Mathf.Sqrt(1- Mathf.Pow(j, 2)/3);
			//y=2.71052f+(1.5f-0.5f*Mathf.Abs(j))-1.35526f * Mathf.Sqrt(4-Mathf.Pow(Mathf.Abs(j)-1,2))*Mathf.Sqrt(Mathf.Abs(Mathf.Abs(j)-1)/(Mathf.Abs(j)-1))+0.9f;

			//sin
			y = Mathf.Sin(j);

			//estrella
			//x= (a - b) * Mathf.Cos(j) + b * Mathf.Cos((a / b - 1) * j);
			//y= (a - b) * Mathf.Sin(j) - b * Mathf.Sin((a / b - 1) * j);
		//for (float j = -Mathf.PI; j < Mathf.PI; j += (Mathf.PI * 2) / 100) {


			//mota
			//y = (1 + 0.9f * Mathf.Cos(8*j)) * (1 + 0.1f * Mathf.Cos (24 * j)) * (0.9f + 0.5f * Mathf.Cos (200 * j)) * (1 + Mathf.Sin (j));
			
			i++;
			GameObject newStep = Instantiate (step, new Vector2 (j, y), Quaternion.identity) as GameObject;
			newStep.transform.SetParent (parent.transform);
			newStep.name = "step-" + i;
		}
	}
}
