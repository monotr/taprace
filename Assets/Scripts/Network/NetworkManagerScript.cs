﻿using UnityEngine;
using System.Collections;

public class NetworkManagerScript : MonoBehaviour {
	const string VERSION = "v0.0.1";
	public string roomName = "principalRoom";
	public string playerPrefName = "PlayerPref";

	void Start () {
		PhotonNetwork.ConnectUsingSettings (VERSION);
		print ("connected");
	}

	void OnJoinedLobby(){
		RoomOptions roomOptions = new RoomOptions () { isVisible = false, maxPlayers = 20 };
		PhotonNetwork.JoinOrCreateRoom (roomName, roomOptions, TypedLobby.Default);
		print ("joined lobby");
	}

	void OnJoinedRoom(){
		print ("joined room");
		//PhotonNetwork.Instantiate (playerPrefName, Vector2.zero, Quaternion.identity, 0);
	}
}
