﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FirebaseCSharp;
using System.Reflection;

public class TapScript : Photon.MonoBehaviour {
	//taps
	public Text tapsTxt, valuePerTapText;
	private double tapsDone, playerCoins;
	public double valuePerTap;

	// number to text
	string[] suffixes = { "", "k", "m", "b", "t", "q" };

	//position
	private GameObject playerObj;
	private double currentMin, currentMax;
	private bool newCurrents = false;
	public Text minTxt, maxTxt;
	public Transform minPos, maxPos, curPos;
	private double curPercPos;
	private float curPercTrans;

	//power
	public float powerTimeMax;
	private float powerTimeCur;
	public Slider powerTimeSlider;
	public Text powerTimeTxt;

	//pass player
	private float passPlayerPoints = 50;

	//powerUps
	Dictionary<double, int> powerUpLocationDictionary; // 0-banana | 1-turtle | 2-stop
	Dictionary<double, GameObject> powerUpObjectDictionary;
	public GameObject[] powerUpsIcons;
	public Text multiplierText;
	private int currentMultiplier = 0;
	private float multiplierTime = 10;
	private bool isTurtle = false;
	private int turtleTapsNeeded = 1;
	private int turtleCurrentTaps = 0;
	public Text turtleTapsTxt;
	private bool isShield = false;
	private float shieldTime = 10;
	private GameObject shieldObj;
	private bool isStop = false;
	public float stopTimer = 5;
	private double casillaStop;
	public SpriteRenderer inkSplatter;
	private double bulletDistanceToAdvance = 50;
	private float autoplayTPS = 10;
	private float autoplaySeconds = 10;
	private bool isParalized = false;

	//online stuff
	public Dictionary<int, SpriteRenderer> onlinePlayers;
	public Text[] playersScoresTxt; // primeros 3, 3 arriba tuyo, 3 abajo tuyo, tu = 10
	private Dictionary<int, string> playersNamesDictionary;
	private Dictionary<int, double> playersScoresDictionary;
	//private Dictionary<int, int> playersPhotonViewDictionary;
	private Dictionary<int, GameObject> playersObjsDictionary;
	public GameObject playerPrefab;
	Color[] playerColors = { Color.red, Color.green, Color.blue, Color.yellow, Color.magenta, Color.gray };
	int myColor;
	bool isFirstPlace = false;
	int totalPlayers = 1;
	//int myPhotonView = 0;

	//road
	public GameObject step;
	private Dictionary<double, Transform> stepPositions;
	private bool isWithEnemy = false;
	private double enemyHP;

	//firebase
	public InputField playerNameInput;
	private bool isSetupComplete = false;
	private bool gotProfile = false;
	private string playerName;
	private int playerID;
	Firebase firebase;
	Firebase frb_child;
	public GameObject profilePanel;

	void Start () {
		PlayerPrefs.SetString("tapsDoneString", "0"); //REMOVE

		//setup online
		firebase = Firebase.CreateNew("tapracing-6f5e4.firebaseio.com");

		//set up variables
		onlinePlayers = new Dictionary<int, SpriteRenderer>();;
		powerUpLocationDictionary = new Dictionary<double, int>();
		stepPositions = new Dictionary<double, Transform>();
		powerUpObjectDictionary = new Dictionary<double, GameObject>();
		playersScoresDictionary = new Dictionary<int, double> ();
		playersNamesDictionary = new Dictionary<int, string> ();
		//playersPhotonViewDictionary = new Dictionary<int, int> ();
		playersObjsDictionary = new Dictionary<int, GameObject> ();
		valuePerTapText.text = valuePerTap + " steps per tap";

		//get saved taps
		/*if (PlayerPrefs.GetString ("tapsDoneString").Length < 1)
			tapsDone = 0;
		else
			tapsDone = double.Parse (PlayerPrefs.GetString ("tapsDoneString"));

		tapsDone = 0; //REMOVE*/

		////TEST!!!!!!!!!	---- ADD BANANAS/TURTLES ON FLOOR
		/*powerUpLocationDictionary.Add (10, 0);
		powerUpLocationDictionary.Add (9, 0);
		for (int i = 0; i < 30; i++) {
			double place = Random.Range (1, 101);
			int kind = Random.Range (0,3);
			if (!powerUpLocationDictionary.ContainsKey (place)) {
				powerUpLocationDictionary.Add (place, kind);
				//print (place + "");
			}
		}*/
		//TEST ENEMY
		powerUpLocationDictionary.Add (7, 4);

		//get min/max values
		//curPercTrans = (Mathf.Abs(minPos.position.x) + Mathf.Abs(maxPos.position.x)) / 100;

		Invoke ("setPlayer", 0.1f);

		//power
		powerTimeCur = powerTimeMax;
		StartCoroutine(powerTime(0.0F));

		//start saving routine
		StartCoroutine(saveTaps(10.0F));
	}

	private void setPlayer(){
		//playerObj = GameObject.FindGameObjectWithTag ("Player");

		if ( gotProfile) { //playerObj != null &&
			//get player name
			playerName = playerNameInput.text;

			//get other players
			StartCoroutine(getOthersPlayers());

			//get player params
			getPlayerDB (playerName);
			if (currentMin == 0) {
				currentMin = 0;
				currentMax = 9;
			}
			//si no tomó ID, es que es nuevo, brinda ID y lo agrega a DB
			if (playerID == 0) {
				playerID = totalPlayers + 1;

			}

			//prints them
			tapsTxt.text = roundNumber (tapsDone);

			profilePanel.SetActive (false);

			//curPos = playerObj.transform;

			if (tapsDone > currentMax || tapsDone < currentMin) {
				newCurrents = true;
				//print (tapsDone + " min: " + currentMin + "  max: " + currentMax);
			}

			configMinMaxPos (tapsDone, true);
			movePlayer ();
			shieldObj = curPos.GetChild (0).gameObject;

			curPos.name = "player-" + playerID;

			//set color
			myColor = Random.Range (0, playerColors.Length);
			curPos.GetComponent<SpriteRenderer> ().color = playerColors [myColor];

			//añade a jugador a la lsita de nombres
			playersNamesDictionary.Add(playerID, playerName);
			//myPhotonView = playerObj.GetComponent<PhotonView> ().viewID;
			//playersPhotonViewDictionary.Add (playerID, myPhotonView);
			updatePlayerDB (playerName, 2, tapsDone);

			//actualiza puntos
			this.photonView.RPC ("updateScore", PhotonTargets.All, tapsDone, playerID);

			//añade a lista de jugadores
			this.photonView.RPC ("addPlayersObj", PhotonTargets.Others, playerID, myColor);

			isSetupComplete = true;

			print ("PLAYER FOUND!!!!!!!!!!");
		} else {
			Invoke ("setPlayer", 0.1f);
			print ("player NOT found");
		}
	}

	//reduce el tiempo en el que se puede jugar
	IEnumerator powerTime(float waitTime) {
		yield return new WaitForSeconds(waitTime);

		//si aun hay tiempo, se resta 1segundo
		if (powerTimeCur > 0) {
			powerTimeCur--;
			powerSlideChange ();
		}

		StartCoroutine(powerTime(1.0F));
	}

	//reduce tiempo de slide
	private void powerSlideChange(){
		powerTimeSlider.value = powerTimeCur / powerTimeMax;
		powerTimeTxt.text = "Energy remaining: " + powerTimeCur;
	}

	//configura min y max de campo
	private void configMinMaxPos(double auxNum, bool isExchange){
		//calcula cantidad de zeros en numero de pisicion
		/*int place = 0;
		while(auxNum >= 10)
		{
			auxNum = auxNum / 10;
			place++;
		}*/

		//obtiene minimo y maximo
		//currentMin = Mathf.Pow (10, place) - 1;	//use this for  0-9 * 9-99 * 99-999 * 999-9999 and so on
		//currentMax = Mathf.Pow (10, place + 1) - 1;

		if (newCurrents) {
			if (!isExchange) {
				currentMin = currentMax + 1;
				currentMax = currentMax + valuePerTap * 100;
			} else {
				currentMin = auxNum;
				currentMax = auxNum + valuePerTap * 100;
			}
			newCurrents = false;
			//currentMax = currentMax + Mathf.Pow (10, place);
		}

		updatePlayerDB (playerName,1, 0);

		//lo imprime
		minTxt.text = roundNumber(currentMin);
		maxTxt.text = roundNumber(currentMax);

		//remove items on floor
		GameObject[] puFloor = GameObject.FindGameObjectsWithTag("PowerUp");
		foreach (GameObject pu in puFloor)
			Destroy (pu);
		//remove players on floor
		GameObject[] playersFloor = GameObject.FindGameObjectsWithTag("OtherPlayer");
		foreach (GameObject pl in playersFloor)
			Destroy (pl);

		int rand = Random.Range (0,2);
		roadConstructor (rand); //construye pista

		//add new items and players on floor
		addItemsPlayersOnFloor();
	}

	public void OnMouseDown(){
		if(isSetupComplete && !isParalized){
			if (!isWithEnemy) { // si no se está enfrentado con enemigo
				if (isTurtle)
					turtleCurrentTaps++;

				if ((powerTimeCur > 0 && !isTurtle) || (powerTimeCur > 0 && isTurtle && turtleCurrentTaps >= turtleTapsNeeded)) {
					//reiniciar taps de turtle
					if (isTurtle)
						turtleCurrentTaps = 0;

					//revisar si pasó por un item
					for (double i = tapsDone + 1; i <= tapsDone + valuePerTap; i++) {
						double newI = System.Math.Floor (i);
						//print ("newI: " + newI);
						if (powerUpLocationDictionary.ContainsKey (newI)) {
							if (!isShield) { //si no está activado escuco, hace daño
								switch (powerUpLocationDictionary [newI]) {
								case 0: // banana
									if (powerTimeCur > 0) {
										powerTimeCur -= 20;
										if (powerTimeCur < 0)
											powerTimeCur = 0;
										powerSlideChange ();
									}
									break;
								case 1: // turtle
									isTurtle = true;
									turtleTapsNeeded++;
									turtleTapsTxt.text = turtleTapsNeeded.ToString ();
									CancelInvoke ("endTurtleEffect");
									Invoke ("endTurtleEffect", 10);
									break;
								case 2: // stop
									isStop = true;
									casillaStop = i;
									//inicia tiempo de paralisis
									isParalized = true;
									Invoke ("unparalizePlayer", stopTimer);
									break;
								case 4: // enemy
									isStop = true;
									casillaStop = i;
									isWithEnemy = true;
									break;
								}
							}

							//destruye item
							if (!isWithEnemy) { // si no es enemy
								this.photonView.RPC ("destroyItem", PhotonTargets.All, newI);
							}

						}
						//PASA A JUGADOR
						if (!isWithEnemy) {// si no es enemigo, revisa si pasó a un jugador, de lo contrario no les hace daño
							if (playersScoresDictionary.ContainsValue (newI)) { //si pasa a un jugador
								var items = from entry in playersScoresDictionary //almacena los jugadores que tengan como valor la posición actual del enemigo
										where entry.Value == newI
									select entry;

								foreach (KeyValuePair<int, double> pair in items) {
									if (pair.Key != playerID) {
										powerTimeCur += passPlayerPoints; //incrementa puntos
										updateScore(pair.Value - 1, pair.Key);
										updatePlayerDB(playersNamesDictionary[pair.Key], 4, pair.Value - 1);
										this.photonView.RPC ("decreasePower", PhotonTargets.Others, pair.Key, 5.0f); // decrementa puntos al rebasado
									}
									break;//lo tuve que poner porque si no marca un error, hay que tener otra forma de obtener una llave de un value
								}
							}
						}
						if (isStop) //si pasa por un stop, no avanza más
						break;
					}

					//incrementar taps
					if (isStop) {
						tapsDone = casillaStop;
						isStop = false;
					} else {
						if (currentMultiplier == 0)
							tapsDone += valuePerTap;
						else
							tapsDone += valuePerTap * currentMultiplier;
					}

					//si llegó a límite, cargar un nuevo nivel
					if (tapsDone > currentMax) {
						newCurrents = true;
						configMinMaxPos (tapsDone, false);
					}
					//mover jugador en mapa
					movePlayer ();

					//actualizar taps text
					tapsTxt.text = roundNumber (tapsDone);

					//ONLINE
					//actualiza puntos
					this.photonView.RPC ("updateScore", PhotonTargets.All, tapsDone, playerID);

					//configura a enemigo
					if (isWithEnemy) //no es lo ideal dejarlo aqui, porque se corre y depende de la función anterior y esta depende del internet
						enemySetup ();
				}
			} else { // si hay batalla con enemigo
				this.photonView.RPC ("damageEnemy", PhotonTargets.All, tapsDone, valuePerTap);
			}
	}
	}

	private void movePlayer(){
		/*//linear
		curPercPos = ((tapsDone - currentMin) / (currentMax - currentMin)) * 100;
		curPos.position = new Vector2 (minPos.position.x + (float)(curPercTrans * curPercPos), minPos.position.y);*/

		//con pista
		curPos.position = stepPositions[tapsDone].position;
	}

	IEnumerator saveTaps(float waitTime) {
		yield return new WaitForSeconds(waitTime);

		//PlayerPrefs.SetString("tapsDoneString", tapsDone.ToString());

		//update to DB
		updatePlayerDB (playerName, 0, 0);

		StartCoroutine(saveTaps(10.0F));
	}

	public string roundNumber(double numerToConvert)
	{
		int place = 0;

		while(numerToConvert >= 1000)
		{
			numerToConvert = numerToConvert / 1000;
			place++;
		}

		//numerToConvert = Mathf.Floor((float)(numerToConvert * 10)) / 10;

		return (numerToConvert).ToString() + suffixes[place];
	}

	//POWER UPS
	public void setItem(string posType){ // agrega power up a escenario = banana, tortuga, alto, bullet, enemy
		string[] parts = posType.Split (',');
		double pos = double.Parse(parts [0]);
		int type = int.Parse (parts[1]);

		if (pos == -1) { //si es agregado con boton, la posición es la del jugador
			pos = tapsDone;
			this.photonView.RPC ("getNewItem", PhotonTargets.Others, pos, type);
		}
		if (powerTimeCur > 0) {
			if (!powerUpLocationDictionary.ContainsKey (pos)) {
				if (pos > currentMin && pos < currentMax) {
					//double bananaPercPos = ((pos - currentMin) / (currentMax - currentMin)) * 100;
					GameObject newItem = Instantiate (powerUpsIcons [type],
											stepPositions[pos].position,
						                    Quaternion.identity) as GameObject;

					if (type == 3) //si es bullet, pone en posición ficticia
						powerUpObjectDictionary.Add (-pos, newItem);
					else // si no, la pone normal
						powerUpObjectDictionary.Add (pos, newItem);
				}

				if (type != 3) //añade a lista a menos que sea bala
					powerUpLocationDictionary.Add (pos, type);
				/*else if (type == 3) {
					StartCoroutine(bulletPU(0.0F));
				}*/

				//no afecta a demás jugadores, de hecho no funciona, ni lo he calado... terminar todo lo de la bullet
			}
		}
	}

	private void endTurtleEffect (){
		turtleCurrentTaps = 0;
		turtleTapsNeeded = 1;
		turtleTapsTxt.text = turtleTapsNeeded.ToString();
		isTurtle = false;
	}

	public void multiplierIncrease(){
		if (powerTimeCur > 0) {
			//valuePerTap *= 2;
			currentMultiplier += 2;
			multiplierText.text = "x" + currentMultiplier;
			valuePerTapText.text = valuePerTap * currentMultiplier + " steps per tap";

			Invoke("multiplierDecrease",multiplierTime);
		}
	}

	private void multiplierDecrease() {
		//valuePerTap /= 2;

		currentMultiplier -= 2;
		multiplierText.text = "x" + currentMultiplier;
		if(currentMultiplier != 0)
			valuePerTapText.text = valuePerTap * currentMultiplier + " steps per tap";
		else
			valuePerTapText.text = valuePerTap + " steps per tap";
	}

	//shield
	public void shieldOn(){
		isShield = true;
		shieldObj.SetActive (true);
		Invoke ("shieldOff", shieldTime);
	}

	private void shieldOff(){
		isShield = false;
		shieldObj.SetActive (false);
	}

	//exchange position
	public void exchangePosition(){
		if (playersScoresDictionary.Count > 1 && !isFirstPlace){ //si hay más de un jugador y no estás en 1er lugar
			List<int> keys = new List<int> (playersScoresDictionary.Keys); //agrega a todos los jugadores
			keys.Remove (playerID); // quita a jugador que usó el power up
			double oldScore = tapsDone; //backup
			while (true) {
				int otherID = keys [Random.Range (0, keys.Count)];
				if (playersScoresDictionary [otherID] > tapsDone) { // si jugador seleccionado tiene más puntos
					this.photonView.RPC ("exchangePlayer", PhotonTargets.Others, otherID, tapsDone); // emvia puntos a otro jugador
					tapsDone = playersScoresDictionary [otherID]; // toma nuevos puntos

					//cambiar min/max
					if (tapsDone > currentMax) {
						newCurrents = true;
						configMinMaxPos (tapsDone, true);
					}
					//mover jugador en mapa
					movePlayer ();

					//actualizar taps text
					tapsTxt.text = roundNumber (tapsDone);

					//ONLINE
					//actualiza puntos
					this.photonView.RPC ("updateScore", PhotonTargets.All, tapsDone, playerID);
					//actualizar puntos de otro jugador en base de datos
					updatePlayerDB(playersNamesDictionary[otherID], 4, oldScore);
					//y en listas
					this.photonView.RPC ("updateScore", PhotonTargets.All, oldScore, otherID);
					break;
				}
			}
		}
	}

	//bullet
	IEnumerator bulletPU(float waitTime, double timesRemaining) {
		yield return new WaitForSeconds(waitTime);

		timesRemaining--;

		//if(timesRemaining > 0)
		//	StartCoroutine(bulletPU(0.2F));
	}

	//ink splatter
	public void inkSplatterPU(){
		this.photonView.RPC ("inkAppears", PhotonTargets.Others);
	}

	//autoplay
	public void autopayON(){
		StartCoroutine(autoplayRoutine(0.0f, autoplaySeconds));
	}

	IEnumerator autoplayRoutine(float waitTime, float seconds) {	//autoplayTPS, autoplaySeconds
		yield return new WaitForSeconds(waitTime);

		OnMouseDown ();

		seconds -= 0.1f;

		if(seconds > 0)
			StartCoroutine(autoplayRoutine(0.1f, seconds));
	}

	//powerups on the foor
	private void addItemsPlayersOnFloor(){
		powerUpObjectDictionary.Clear();
		playersObjsDictionary.Clear();
		for (double i = currentMin; i < currentMax + 1; i++) {
			//adds items
			if (powerUpLocationDictionary.ContainsKey (i)) {
				GameObject newItem = Instantiate (powerUpsIcons [powerUpLocationDictionary[i]],
											stepPositions[i].position,
					                      Quaternion.identity) as GameObject;
				powerUpObjectDictionary.Add (i, newItem);
			}

			//add players
			if (playersScoresDictionary.ContainsValue (i)) {
				var keys = from entry in playersScoresDictionary //almacena los jugadores que tengan como valor la posición actual del enemigo
				where entry.Value == i
				           select entry.Key;
				foreach (var key in keys) { // agrega a todos los jugadores que estén en la misma posición
					if (key != playerID) {
						GameObject newPlayer = Instantiate (playerPrefab, stepPositions [i].position, Quaternion.identity) as GameObject;
						playersObjsDictionary.Add (key, newPlayer);
					}
				} 
			}

		}
	}


	//ONLINE STUFF
	//obtiene items puestos por otro jugador
	[PunRPC]
	void getNewItem(double pos, int type)
	{
		string par = pos.ToString () + "," + type.ToString ();
		setItem (par);
		//print ("sent item");
	}

	//destruye un item tomado por otro jugador
	[PunRPC]
	void destroyItem(double pos)
	{
		try{Destroy (powerUpObjectDictionary [pos]);}	catch{}
		powerUpObjectDictionary.Remove (pos);
		powerUpLocationDictionary.Remove (pos);
		//print ("destroyed item");
	}

	//
	[PunRPC]
	void updateScore(double otherPlayerScore, int _playerID)
	{
		if (!playersScoresDictionary.ContainsKey (_playerID)) //si aun no está en score, agrégalo
			playersScoresDictionary.Add (_playerID, otherPlayerScore);
		else // si ya está, actualiza
			playersScoresDictionary [_playerID] = otherPlayerScore;

		//ordena por puntuación
		var items = from pair in playersScoresDictionary
			orderby pair.Value descending
			select pair;

		int i = 0;

		foreach (KeyValuePair<int, double> pair in items)
		{
			if (i == 0) { // revisa si estás en 1er lugar
				if (pair.Key == playerID)
					isFirstPlace = true;
				else
					isFirstPlace = false;
			}

			if(i <= 5) // imprime primeros 6 lugares solamente
				playersScoresTxt[i].text = "(" + (i+1) + ") - " + playersNamesDictionary[pair.Key] + " - " + pair.Value + " taps";
			
			i++;

			//revisa si está en mismo nivel, si no, lo oculta
			//if (onlinePlayers.ContainsKey (pair.Key) && pair.Key != playerID) {
			if (pair.Value >= currentMin && pair.Value <= currentMax && pair.Key != playerID) { //si está dentro
				if (!playersObjsDictionary.ContainsKey (pair.Key)) {
					GameObject newPlayer = Instantiate (playerPrefab, stepPositions [pair.Value].position, Quaternion.identity) as GameObject;
					playersObjsDictionary.Add (pair.Key, newPlayer);
				} else {
					playersObjsDictionary [pair.Key].transform.position = stepPositions [pair.Value].position;
				}
			}
			else if(playersObjsDictionary.ContainsKey (pair.Key)){
				Destroy (playersObjsDictionary [pair.Key]);
				playersObjsDictionary.Remove (pair.Key);
			}
					/*if (!onlinePlayers [pair.Key].enabled)
						onlinePlayers [pair.Key].enabled = true;
				} else if (onlinePlayers [pair.Key].enabled && onlinePlayers [pair.Key].enabled)
					onlinePlayers [pair.Key].enabled = false;*/
			//}
		}
		playersScoresTxt[6].text = "LAST~ (" + items.Count() + ") - " + playersNamesDictionary[items.Last().Key] + " - " + items.Last().Value.ToString() + " taps";

	}

	//agrega nuevos jugadores a lista
	[PunRPC]
	void addPlayersObj(int userID, int colorP)
	{
		if (!playersScoresDictionary.ContainsKey (userID)) { //si aun no está en score, agrégalo
			playersScoresDictionary.Add (userID, 0);
		}
		/*if (!onlinePlayers.ContainsKey (userID)) {
			GameObject newPlayer = PhotonView.Find (playersPhotonViewDictionary[userID]).gameObject;
			SpriteRenderer playerSR = newPlayer.GetComponent<SpriteRenderer>();
			playerSR.color = playerColors [colorP];
			newPlayer.name = "player-" + userID;
			onlinePlayers.Add (userID, playerSR);
			onlinePlayers [userID].enabled = false;

			if (!playersScoresDictionary.ContainsKey (userID)) { //si aun no está en score, agrégalo
				playersScoresDictionary.Add (userID, 0);
			}
			print ("added new player-" + userID);
		}*/
	}

	//decreasePower
	[PunRPC]
	void decreasePower(int userID, float paralizeTime)
	{
		if (playerID == userID) {
			powerTimeCur -= passPlayerPoints; //decrementa puntos

			tapsDone--; //retrocede un lugar

			//mover jugador en mapa
			movePlayer ();

			//actualizar taps text
			tapsTxt.text = roundNumber (tapsDone);

			//inicia tiempo de paralisis
			isParalized = true;
			Invoke ("unparalizePlayer", paralizeTime);

			//ONLINE
			//actualiza puntos
			this.photonView.RPC ("updateScore", PhotonTargets.All, tapsDone, playerID);
		}
	}

	private void unparalizePlayer(){
		isParalized = false;
	}

	//exchangePlayer
	[PunRPC]
	void exchangePlayer(int userID, double newPoints)
	{
		if (playerID == userID) {
			tapsDone = newPoints; //ingresa nuevos puntos

			//cambiar min/max
			if (tapsDone < currentMin) {
				newCurrents = true;
				configMinMaxPos (tapsDone, true);
			}
			//mover jugador en mapa
			movePlayer ();

			//actualizar taps text
			tapsTxt.text = roundNumber (tapsDone);

			//ONLINE
			//actualiza puntos ** ya no porque lo hace el que usa el PowerUp
			//this.photonView.RPC ("updateScore", PhotonTargets.All, tapsDone, playerID);
		}
	}

	//ink Splatter
	[PunRPC]
	void inkAppears()
	{
		inkSplatter.color = new Color(1, 1, 1, 1);

		StartCoroutine(inkFades(0.1F));
	}

	IEnumerator inkFades(float waitTime) {
		yield return new WaitForSeconds(waitTime);

		inkSplatter.color -= new Color(0, 0, 0, 0.05f);

		if(inkSplatter.color.a > 0)
			StartCoroutine(inkFades(0.2F));
	}

	//ENEMY
	//ENEMY Setup
	void enemySetup(){
		int playersThere = 0;

		var keys = from entry in playersScoresDictionary //almacena los jugadores que tengan como valor la posición actual del enemigo
				where entry.Value == tapsDone 
			select entry.Key;
		//print (tapsDone);
		foreach (var key in keys) { // por cada jugador, suma 1
			//print (key);
			playersThere++;
		}

		if (playersThere <= 1) { //si solo hay un jugador, le da HP al enemigo
			enemyHP = valuePerTap * 10;
			//print ("you just arrived");
		} else {		// si ya habia otro jugador, le pide el HP
			this.photonView.RPC ("getEnemyHP", PhotonTargets.Others, tapsDone, playerID);
		}
	}
	//Get enemy HP from other player
	[PunRPC]
	void getEnemyHP(double enemyPos, int otherID){
		if (enemyPos == tapsDone && isWithEnemy)
			this.photonView.RPC ("setEnemyHP", PhotonTargets.Others, enemyHP, otherID);
	}
	//set HP to requested player
	[PunRPC]
	void setEnemyHP(double currentEnemyHP, int otherID){
		if(playerID == otherID)
			enemyHP = currentEnemyHP;
	}

	//DAMAGE
	[PunRPC]
	void damageEnemy(double enemyPos, double damageDone){
		if (enemyPos == tapsDone && isWithEnemy) {
			enemyHP -= damageDone;
			print ("enemy HP: " + enemyHP);
			if (enemyHP <= 0) { // si no es enemy
				this.photonView.RPC ("destroyItem", PhotonTargets.All, enemyPos); //quita de escenario y listas a objeto
				isWithEnemy = false; // ya no está luchando
			}
		}
	}


	//ROAD GENERATOR
	private void roadConstructor(int roadNum){
		stepPositions.Clear ();
		Destroy(GameObject.Find ("roadParent"));

		GameObject parent = new GameObject ();
		parent.name = "roadParent";
		int i = 0;
		float z = 0;

		float divisor = 0;

		if (currentMin == 0)
			divisor = 10;
		else
			divisor = 100;

		for(float j = 0; j < 20; j += 20/divisor){
			switch(roadNum){
			case 0:
				z = Mathf.Sin (j);
				break;
			case 1:
				z = Mathf.Cos (j);
				break;
			}

			GameObject newStep = Instantiate (step, new Vector3 (z*4.5f, 1, j), Quaternion.identity) as GameObject;
			newStep.transform.SetParent (parent.transform);
			newStep.name = "step-" + (currentMin + (i * valuePerTap));
			stepPositions.Add (currentMin + (i * valuePerTap), newStep.transform);
			i++;
		}
	}

	//get profile info
	public void getName(){
		gotProfile = true;
	}

	//FIREBASE
	public void updatePlayerDB(string _playerName, int typeOfUpdate, double _newScore){ 
		//config db
		frb_child = firebase.Child(_playerName);

		frb_child.OnFetchSuccess += GetOKHandler;
		frb_child.OnFetchFailed += GetFailHandler;
		frb_child.OnUpdateSuccess += SetOKHandler;
		frb_child.OnUpdateFailed += SetFailHandler;
		frb_child.OnPushSuccess += PushOKHandler;
		frb_child.OnPushFailed += PushFailHandler;

		frb_child.OnDeleteSuccess += DelOKHandler;
		frb_child.OnDeleteFailed += DelFailHandler;

		//set param
		switch (typeOfUpdate) { //typeOfUpdate = 0 - player taps, 1 - player max/min, 2 - todo, 3 - monedas, 4- other player
		case 0:
			firebase.Child(_playerName).Child("score").SetValue(tapsDone);
			break;
		case 1:
			firebase.Child(_playerName).Child("score").SetValue(tapsDone);
			firebase.Child(_playerName).Child("currentMax").SetValue(currentMax);
			firebase.Child(_playerName).Child("currentMin").SetValue(currentMin);
			break;
		case 2:
			Dictionary<string, object> playerDict = new Dictionary<string, object> ();

			Dictionary<string, object> playerParam = new Dictionary<string, object> ();
			playerParam.Add ("score", tapsDone);
			playerParam.Add ("money", playerCoins);
			playerParam.Add ("currentMax", currentMax);
			playerParam.Add ("currentMin", currentMin);
			playerParam.Add ("userID", playerID);
			//playerParam.Add ("photonView", myPhotonView);
			playerDict.Add (_playerName, playerParam);

			StartCoroutine (Firebase.ToCoroutine (firebase.SetValue, playerDict));
			break;
		case 3:
			firebase.Child(_playerName).Child("money").SetValue(playerCoins);
			break;
		case 4:
			firebase.Child(_playerName).Child("score").SetValue(_newScore);
			break;
		}


	}

	public void getPlayerDB(string playerName){
		//config db
		frb_child = firebase.Child(playerName);

		frb_child.OnFetchSuccess += GetOKHandler;
		frb_child.OnFetchFailed += GetFailHandler;
		frb_child.OnUpdateSuccess += SetOKHandler;
		frb_child.OnUpdateFailed += SetFailHandler;
		frb_child.OnPushSuccess += PushOKHandler;
		frb_child.OnPushFailed += PushFailHandler;

		frb_child.OnDeleteSuccess += DelOKHandler;
		frb_child.OnDeleteFailed += DelFailHandler;

		//get param
		StartCoroutine(Firebase.ToCoroutine(frb_child.GetValue));
	}

	//ON EXIT APP
	void OnApplicationQuit(){
		updatePlayerDB (playerName, 2, 0);
	}

	#region firebase
	void OnDisable()
	{
		frb_child.OnFetchSuccess -= GetOKHandler;
		frb_child.OnFetchFailed -= GetFailHandler;
		frb_child.OnUpdateSuccess -= SetOKHandler;
		frb_child.OnUpdateFailed -= SetFailHandler;
		frb_child.OnPushSuccess -= PushOKHandler;
		frb_child.OnPushFailed -= PushFailHandler;

		frb_child.OnDeleteSuccess -= DelOKHandler;
		frb_child.OnDeleteFailed -= DelFailHandler;
	}

	void GetOKHandler(Firebase sender, DataSnapshot snapshot)
	{
		DoDebug("[OK] Get from key: <" + sender.FullKey + ">");
		DoDebug("[OK] Raw Json: " + snapshot.RawJson);

		Dictionary<string, object> dict = snapshot.Value<Dictionary<string, object>>();
		List<string> keys = snapshot.Keys;

		if (keys != null)
			foreach (string key in keys)
			{
				print ("KEY NOW: " + key);
				if (key == "money")
					playerCoins = System.Convert.ToDouble (dict [key]);
				else if (key == "score")
					tapsDone = System.Convert.ToDouble (dict [key]);
				else if (key == "currentMin")
					currentMin = System.Convert.ToDouble (dict [key]);
				else if (key == "currentMax")
					currentMax = System.Convert.ToDouble (dict [key]);
				else if (key == "userID")
					playerID = System.Convert.ToInt32 (dict [key]);
				else if (key != playerName && key != "photonView"){
					var thisPlayer = dict[key] as Dictionary<string, object>;
					//addPlayersObj (System.Convert.ToInt32(thisPlayer["userID"]), 1);
					totalPlayers++;
					playersNamesDictionary.Add(System.Convert.ToInt32(thisPlayer["userID"]), key);
					//playersPhotonViewDictionary.Add (System.Convert.ToInt32(thisPlayer["userID"]), System.Convert.ToInt32(thisPlayer["photonView"]));
					playersScoresDictionary.Add (System.Convert.ToInt32(thisPlayer["userID"]), System.Convert.ToDouble (thisPlayer["score"]));
					//print ("added ID/Name: " + System.Convert.ToInt32(thisPlayer["userID"]) + " / " + key);
					//updateScore(System.Convert.ToDouble (thisPlayer["score"]), System.Convert.ToInt32(thisPlayer["userID"]));
					//print(System.Convert.ToInt32(thisPlayer["userID"]) + " score:  " + System.Convert.ToDouble(thisPlayer["score"]));
					//IDictionary<string, object> objectBackToDictionary = ObjectExtensions.AsDictionary (dict [key]);
					//no funciona, hay que lograr que entre a convertir a diccionario solo cuando son jugadores
					//List<string> keysP = objectBackToDictionary.Keys;
					//print ("@@@" + objectBackToDictionary.Count());
				}

				DoDebug(key + " = " + dict[key].ToString());
			}
	}

	void GetFailHandler(Firebase sender, FirebaseError err)
	{
		DoDebug("[ERR] Get from key: <" + sender.FullKey + ">,  " + err.Message + " (" + (int)err.Status + ")");
	}

	void SetOKHandler(Firebase sender, DataSnapshot snapshot)
	{
		DoDebug("[OK] Set from key: <" + sender.FullKey + ">");
	}

	void SetFailHandler(Firebase sender, FirebaseError err)
	{
		DoDebug("[ERR] Set from key: <" + sender.FullKey + ">, " + err.Message + " (" + (int)err.Status + ")");
	}

	void DelOKHandler(Firebase sender, DataSnapshot snapshot)
	{
		DoDebug("[OK] Del from key: <" + sender.FullKey + ">");
	}

	void DelFailHandler(Firebase sender, FirebaseError err)
	{
		DoDebug("[ERR] Del from key: <" + sender.FullKey + ">, " + err.Message + " (" + (int)err.Status + ")");
	}

	void PushOKHandler(Firebase sender, DataSnapshot snapshot)
	{
		DoDebug("[OK] Push from key: <" + sender.FullKey + ">");
	}

	void PushFailHandler(Firebase sender, FirebaseError err)
	{
		DoDebug("[ERR] Push from key: <" + sender.FullKey + ">, " + err.Message + " (" + (int)err.Status + ")");
	}

	void DoDebug(string str)
	{
		//Debug.Log(str);
		print(str);

	}

	//get other players
	IEnumerator getOthersPlayers()
	{
		firebase.OnFetchSuccess += GetOKHandler;
		firebase.OnFetchFailed += GetFailHandler;
		firebase.OnUpdateSuccess += SetOKHandler;
		firebase.OnUpdateFailed += SetFailHandler;
		firebase.OnPushSuccess += PushOKHandler;
		firebase.OnPushFailed += PushFailHandler;

		firebase.OnDeleteSuccess += DelOKHandler;
		firebase.OnDeleteFailed += DelFailHandler;

		//firebase.GetValue();
		string estring = "score";
		StartCoroutine(Firebase.ToCoroutine(firebase.GetValue, FirebaseParam.Empty.StartAt(playerName).OrderByChild(estring).LimitToFirst(3)));
		//StartCoroutine(Firebase.ToCoroutine(frb_child.GetValue, FirebaseParam.Empty.OrderByKey().StartAt(playerName).LimitToFirst(10)));

		yield return null;
	}
	#endregion
}
